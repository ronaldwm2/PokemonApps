/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import type {PropsWithChildren} from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  View,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

import GlobalStyle from './globalstyle';
import { Provider } from 'react-redux';
import store_config from './stateService/store_config';
import { PokemonList } from './Template/pokemonlist';
import { PokemonDetail } from './Template/pokemonDetail';
import { PokemonStorage } from './Template/pokemonStorage';

function App(): JSX.Element {

  return (
    <Provider store={store_config}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="HomeScreen">
          <Stack.Screen
            name="HomeScreen"
            component={PokemonList}
            options= { ({navigation}) => ({ 
              headerStyle: GlobalStyle.mainColor,
              headerTitleStyle: GlobalStyle.mainColorText,
              headerTintColor: GlobalStyle.mainColorText.color,
              title: "Pokedex Apps",
              headerRight: () => (
                <Button
                  onPress={() => {
                    navigation.navigate("StorageScreen")
                  }}
                  title="My Pokemon List"
                  color="#000"
                />
              )
            })}
          />
          <Stack.Screen
            name="DetailScreen"
            component={PokemonDetail}
            options={{ 
              headerStyle: GlobalStyle.mainColor,
              headerTitleStyle: GlobalStyle.mainColorText,
              headerTintColor: GlobalStyle.mainColorText.color
            }}
          ></Stack.Screen>
          <Stack.Screen
            name="StorageScreen"
            component={PokemonStorage}
            options={{ 
              headerStyle: GlobalStyle.mainColor,
              headerTitleStyle: GlobalStyle.mainColorText,
              headerTintColor: GlobalStyle.mainColorText.color
            }}
          ></Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}


export default App;
