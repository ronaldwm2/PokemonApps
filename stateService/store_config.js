import { configureStore, combineReducers } from '@reduxjs/toolkit'
import pokemonDetailState from './pokemonDetailState'
import pokemonListState from './pokemonListState'
import pokemonStorageState from './pokemonStorageState'

const combinedReducer = combineReducers({
  pokemonListState: pokemonListState,
  pokemonDetailState: pokemonDetailState,
  pokemonStorageState: pokemonStorageState
})

const rootReducer = (state, action) => {
  // if (action.type === 'pokemonDetailState/clearDetail') {

  // }
  return combinedReducer(state, action);
}

export default configureStore({
  reducer: rootReducer
})