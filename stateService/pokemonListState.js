import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { client } from '../apiService/client'
import { ToastAndroid } from 'react-native'

export const fetchAllPokemon = createAsyncThunk('https://pokeapi.co/api/v2/pokemon?limit=40&offset=0', async () => {
    const response = await client.get('https://pokeapi.co/api/v2/pokemon?limit=40&offset=0')
    // console.log("masuk sini ga", response.data.results);
    return response.data.results;
})

export const fetchNextPage = createAsyncThunk('https://pokeapi.co/api/v2/pokemon?limit=40&offset=', async (pages) => {
    const response = await client.get('https://pokeapi.co/api/v2/pokemon?limit=40&offset=' + pages * 40)
    return response.data.results;
})

export const fetchAllPokemonNoLimit = createAsyncThunk('https://pokeapi.co/api/v2/pokemon?limit=100000', async () => {
    const response = await client.get('https://pokeapi.co/api/v2/pokemon?limit=100000')
    // console.log("masuk sini ga", response.data.results);
    return response.data.results;
})

const initialState = {
    isLoading: false,
    isFetched: false,
    lstOfPokemonList: [],
    isError: false,
    isSearchAll: false,
    page: 0
};

const pokemonListState = createSlice({
    name: 'pokemonListState',
    initialState,
    reducers: {
        filterLstOfPokemon: (state, searchParam) => {
            state.lstOfPokemonList = state.lstOfPokemonList.filter((e) => {
                return e.name.toLowerCase().includes(searchParam.toLowerCase());
            })
        },
        emptyLstOfPokemon: (state) => {
        },
        resetPokemonList: (state) => {
            ToastAndroid.show("Refetching data", ToastAndroid.SHORT);
            state.lstOfPokemonList = initialState;
        }
    },
    extraReducers(builder) {
        builder.addCase(fetchAllPokemon.pending, (state, action) => {
            state.isLoading = true;
            state.isError = false;
            state.isFetched = false;
            state.isSearchAll = false;
        })
        .addCase(fetchAllPokemon.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isError = false;
            state.isFetched = true;
            state.isSearchAll = false;
            state.page++;
            state.lstOfPokemonList.push(...action.payload);
        })
        .addCase(fetchNextPage.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isError = false;
            state.isFetched = true;
            state.page++;
            state.lstOfPokemonList.push(...action.payload);
        })
        .addCase(fetchAllPokemon.rejected, (state, action) => {
            state.isLoading = false;
            state.isFetched = true;
            state.isError = true;
            state.isSearchAll = false;
        })
        .addCase(fetchAllPokemonNoLimit.pending, (state, action) => {
            state.isLoading = true;
            state.isError = false;
            state.isFetched = false;
            state.page = 0;
            state.isSearchAll = true;
        })
        .addCase(fetchAllPokemonNoLimit.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isError = false;
            state.isFetched = true;
            state.isSearchAll = true;
            state.lstOfPokemonList = action.payload;
        })
        .addCase(fetchAllPokemonNoLimit.rejected, (state, action) => {
            state.isLoading = false;
            state.isFetched = true;
            state.isError = true;
            state.isSearchAll = false;
        })
    }
})

export const { filterLstOfPokemon, resetPokemonList } = pokemonListState.actions;
 
export default pokemonListState.reducer;

export const getCurrentPage = state => state.pokemonListState.page;
export const getAllPokemons = state => state.pokemonListState.lstOfPokemonList;
export const checkIsListingLoading = state => state.pokemonListState.isLoading;
export const checkIsListingError = state => state.pokemonListState.isError;