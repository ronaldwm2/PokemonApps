import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { client } from '../apiService/client'

export const fetchPokeDetailById = createAsyncThunk('https://pokeapi.co/api/v2/pokemon/', async (id) => {
    const response = await client.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
    // console.log("masuk sini ga", response.data.results);
    return response.data.results;
})

export const fetchPokeDetailByFullUrl = createAsyncThunk('https://pokeapi.co/api/v2/pokemon/id', async (url) => {
    const response = await client.get(url)
    let data = response.data;
    const pokemonDetails = {
        image_url: data.sprites.front_default,
        name: data.name,
        abilities: data.abilities,
        stats: data.stats,
        types: data.types,
        url: url,
        id: data.id
    }
    return pokemonDetails;
})


const initialState = {
    isLoading: false,
    isFetched: false,
    pokeInfo: {},
    isError: false,
    isImageLoaded: false
};

const pokemonDetailState = createSlice({
    name: 'pokemonDetailState',
    initialState,
    reducers: {
        setImageLoaded: (state, searchParam) => {
            state.isImageLoaded = searchParam;
        },
        clearDetail: (state) => {
            state.isLoading = false;
            state.isFetched = false;
            state.pokeInfo = {};
            state. isError = false;
            state.isImageLoaded = false;
        }
    }, 
    extraReducers(builder) {
        builder.addCase(fetchPokeDetailByFullUrl.pending, (state, action) => {
            state.isLoading = true;
            state.isError = false;
            state.isFetched = false;
        })
        .addCase(fetchPokeDetailByFullUrl.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isError = false;
            state.isFetched = true;
            state.pokeInfo = action.payload;
        })
        .addCase(fetchPokeDetailByFullUrl.rejected, (state, action) => {
            state.isLoading = false;
            state.isFetched = true;
            state.isError = true;
        })
    }
})

export const { setImageLoaded, clearDetail } = pokemonDetailState.actions;
 
export default pokemonDetailState.reducer;
