import { ToastAndroid } from 'react-native'
import { createSlice } from '@reduxjs/toolkit'

function generateRandomIntegerInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const initialState = {
    ownedPokemon: [],
    catchOnProgress: false
};

const pokemonStorageState = createSlice({
    name: 'pokemonStorageState',
    initialState,
    reducers: {
        removeOwnedPokemon: (state, searchParam) => {
            let idx = state.ownedPokemon.findIndex((e) => e.id == searchParam.payload);
            console.log("idx", idx);
            state.ownedPokemon.splice(idx, 1);
            ToastAndroid.show("Success Release Pokemon!", ToastAndroid.SHORT);
        },
        catchPokemon: (state, param) => {
            state.catchOnProgress = false;
            let rng = generateRandomIntegerInRange(0,100);
            if (rng > 50) {
                ToastAndroid.show("Catch success!", ToastAndroid.SHORT);
                state.ownedPokemon.push(param.payload);
            } else {
                ToastAndroid.show("Catch failed!", ToastAndroid.SHORT)
            }
        },
        setCatchIsInProgress: (state) => {
            state.catchOnProgress = true;
        }
    }
})

export const tryCatchingPokemons = (pokeInfo) => (dispatch) => {
    dispatch(setCatchIsInProgress());
    setTimeout(() => {
        dispatch(catchPokemon(pokeInfo));
    }, 1500)
}

export const { removeOwnedPokemon, catchPokemon, setCatchIsInProgress } = pokemonStorageState.actions;

 
export default pokemonStorageState.reducer;