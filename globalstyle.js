import { StyleSheet } from "react-native";

const GlobalStyle = StyleSheet.create({
    mainColor: {
        backgroundColor: "#154c79",
        color: 'white'
    },
    mainColorText: {
        color: 'white'
    },
    secondaryColor: {
        backgroundColor: "#154c79",
        color: 'white'
    },
    mainColorText: {
        color: 'white'
    },
    paddingMedium: {
        padding: 8
    },
    marginMedium: {
        margin: 8
    },
    textGap: {
        marginBottom: 6
    },
    paddingMediumRight: {
        paddingRight: 4
    },
    idLength: {
        width: 75
    },
    verticalCenter: {
        alignSelf: "center"
    },
    flexContainernonFlex1: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    flexContainer: {
        flex:1,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    fullWidth: {
        flex:1,
        alignSelf: 'stretch',
        textAlign: "center",
        width: 200
    },
    separatorUnderline: {
        borderBottomWidth: 1
    },
    smallSearchBorder: {
        borderWidth: 0.5,
        margin: 0,
        padding: 2
    },
    paddingtbmedium: {
        paddingTop: 4,
        paddingBottom: 4
    },
    paddingtblarge: {
        paddingTop: 12,
        paddingBottom: 12
    },
    centerText: {
        textAlign: "center"
    },
    bigHeaderText: {
        color: "black",
        fontSize: 32,
        paddingBottom: 15
    },
    detailsImage: {
        width: 250,
        height: 250,
        overflow: "hidden",
        borderWidth: 1,
        borderColor: "gray",
        display: "flex"
    },
    blackColor: {
        color: "black"
    }
})

export default GlobalStyle;