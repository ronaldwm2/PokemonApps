import React, { memo, useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { filterLstOfPokemon, fetchAllPokemon, getAllPokemons, checkIsListingLoading, checkIsListingError, fetchNextPage, fetchAllPokemonNoLimit, resetPokemonList } from '../stateService/pokemonListState';
import { Button, FlatList, Text, TextInput, View, TouchableNativeFeedback } from 'react-native';
import GlobalStyle from '../globalstyle';
import { ScrollView } from 'react-native-gesture-handler';
import { clearDetail } from '../stateService/pokemonDetailState';

const RenderSingles = memo(
    function RenderSingles({item, navigation, dispatch}) {
    return (
        <TouchableNativeFeedback
            onPress={(e) => {
                dispatch(clearDetail());
                navigation.navigate('DetailScreen', {
                    url: item.url
                })
            }}>
            <View style={{...GlobalStyle.separatorUnderline ,...GlobalStyle.flexContainer, ...GlobalStyle.paddingtblarge}}>
                <View style={{ ...GlobalStyle.paddingMediumRight, ...GlobalStyle.idLength, ...GlobalStyle.verticalCenter }}><Text style={{...GlobalStyle.blackColor}}>{item.url.replace(/.*\/([a-zA-Z]*)\//, "").replace("/","")}</Text></View>
                <View style={{ ...GlobalStyle.fullWidth}}><Text style={{...GlobalStyle.blackColor}} key={item.name}>{item.name}</Text></View>
            </View>
        </TouchableNativeFeedback>
        // <View style={{...GlobalStyle.separatorUnderline ,...GlobalStyle.flexContainer, ...GlobalStyle.marginMedium, ...GlobalStyle.paddingtbmedium}}>
        //     <View style={{ ...GlobalStyle.paddingMediumRight, ...GlobalStyle.idLength, ...GlobalStyle.verticalCenter }}><Text style={{}}>{item.url.replace(/.*\/([a-zA-Z]*)\//, "").replace("/","")}</Text></View>
        //     <View style={{ ...GlobalStyle.fullWidth}}><Text style={{}} key={item.name}>{item.name}</Text></View>
        // </View>

    )
});

export const PokemonList = ({ navigation }) => {
    const [searchItem, setSearchItem] = useState('');
    const [isSearchDone, setIsSearchDone] = useState(true);
    const dispatch = useDispatch();
    const lstOfPokemon = useSelector(getAllPokemons);
    const isAPILoading = useSelector(checkIsListingLoading);
    const isAPIError = useSelector(checkIsListingError);
    const isAPIFetched = useSelector(state => state.pokemonListState.isFetched );
    const pages = useSelector(state => state.pokemonListState.page );
    const isSearchAll = useSelector(state => state.pokemonListState.isSearchAll );
    useEffect(() => {
        // dispatch(fetchAllPokemon());
        if (isAPILoading == false && isAPIFetched == false) {
            dispatch(fetchAllPokemon());
        }
    }, [isAPILoading, dispatch]);
    let contentToShow;
    let searchController: any = null;
    if (!isSearchDone) {
        searchController = setTimeout(() => {
            setIsSearchDone(true);
            if (searchItem == "") {
                dispatch(fetchAllPokemon());
            } else {
                if (!isSearchAll) {
                    dispatch(fetchAllPokemonNoLimit())
                }
            }
        }, 1000)
    }

    if (!isSearchDone) {
        contentToShow = (<Text style={{...GlobalStyle.blackColor}}>Loading....</Text>);
    } else if (isAPILoading) {
        contentToShow = (<Text style={{...GlobalStyle.blackColor}}>Loading....</Text>);
    } else if (isAPIError) {
        contentToShow = (<View><Text style={{...GlobalStyle.blackColor}}>There is error when fetching the data</Text><Button onPress={
            () => {
                dispatch(resetPokemonList());
            }
        } title="Retry"/></View>)
    } else {
        // contentToShow = lstOfPokemon.map(pokemon => (
            
            
        // ))
        let dataToShow;
        if (searchItem == "") {
            dataToShow = lstOfPokemon
        } else {
            dataToShow = lstOfPokemon.filter((e) => {
                return e.name.toLowerCase().includes(searchItem.toLowerCase())
            });
        }
        contentToShow = (
            <FlatList
                contentContainerStyle={{
                    // backgroundColor: 'white',
                    // padding: 20,
                }}
                data={dataToShow}
                renderItem={({ item} ) => {
                    return (<RenderSingles
                        item={item}
                        navigation={navigation}
                        dispatch={dispatch}>

                    </RenderSingles>);
                }}
                keyExtractor={(item, index) => index.toString()}
                onEndReached={(data) => {
                    if (searchItem == "") {
                        dispatch(fetchNextPage(pages))
                    }
                    
                }}
                maxToRenderPerBatch={20}
            />
        )
    }

    return (
            <View style={{...GlobalStyle.paddingMedium}}>
                <Text style={{...GlobalStyle.blackColor}}>Welcome to Pokedex Apps!</Text>
                <Text style ={{...GlobalStyle.textGap, ...GlobalStyle.blackColor}}>Click pokemon below if you want to know their details</Text>
                <Text style={{...GlobalStyle.blackColor}}>Search</Text>
                <TextInput
                    label="Search Param"
                    style= {GlobalStyle.smallSearchBorder, GlobalStyle.blackColor}
                    placeholder="Search Input"
                    placeholderTextColor="#A9A9A9" 
                    value={searchItem}
                    onChangeText={text => {
                        setSearchItem(text);
                        setIsSearchDone(false);

                        clearTimeout(searchController);
                    }}
                />
                {contentToShow}
            </View>
            
        
    )
}