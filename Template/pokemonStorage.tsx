import { Button, Text, TouchableNativeFeedback, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { useSelector, useDispatch } from 'react-redux';
import GlobalStyle from "../globalstyle";
import { clearDetail } from "../stateService/pokemonDetailState";
import { removeOwnedPokemon } from "../stateService/pokemonStorageState";



export const PokemonStorage = ({ navigation }) => {
    navigation.setOptions({
        title: "My Pokemon List"
    })
    const dispatch = useDispatch();
    const lstOfFetched = useSelector(state => state.pokemonStorageState.ownedPokemon);
    let dataToShow = [];
    let idx = 0;
    for (let data of lstOfFetched) {
        ++idx;
        dataToShow.push(
        <TouchableNativeFeedback
            key={idx}
            onPress={(e) => {
                dispatch(clearDetail());
                navigation.navigate('DetailScreen', {
                    url: data.url
                })
            }}>
            <View style={{...GlobalStyle.separatorUnderline ,...GlobalStyle.flexContainernonFlex1, ...GlobalStyle.paddingtblarge}}>
                <View style={{ ...GlobalStyle.paddingMediumRight, ...GlobalStyle.idLength, ...GlobalStyle.verticalCenter }}><Text style={{...GlobalStyle.centerText, ...GlobalStyle.blackColor}}>{idx}</Text></View>
                <View style={{ ...GlobalStyle.fullWidth, ...GlobalStyle.verticalCenter}}><Text style={{...GlobalStyle.blackColor}}>{data.name}</Text></View>
                <View style={{ ...GlobalStyle.paddingMediumRight, width: "40%", ...GlobalStyle.verticalCenter }}>
                    <Button
                        onPress={() => {
                            dispatch(removeOwnedPokemon(data.id))
                        }}
                        title="Release Pokemons"
                        color="#000"
                    />
                </View>
            </View>
        </TouchableNativeFeedback>
        )
    }
    return (
    <ScrollView>
        <View
            style={{flexDirection:"column"}}>
                {dataToShow}
        </View>
    </ScrollView>
    );
}