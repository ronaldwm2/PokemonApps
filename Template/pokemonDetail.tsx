import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { ScrollView, Button, FlatList, Image, StyleSheet, Text, TextInput, TouchableNativeFeedback, View } from 'react-native';
import GlobalStyle from '../globalstyle';
import { fetchPokeDetailByFullUrl, setImageLoaded } from '../stateService/pokemonDetailState';
import { catchPokemon, tryCatchingPokemons } from '../stateService/pokemonStorageState';

const localCss = StyleSheet.create({
    statsSingle: {
        width: "50%", alignContent:"center", justifyContent:"center", alignItems:"center", flexDirection:"column", marginBottom:7.5
    },
    statsHeader: {
        fontWeight: "bold",
        fontSize: 16,
        color: "black"
    }
})

export const PokemonDetail = ({route, navigation}) => {
    const {url} = route.params;
    const dispatch = useDispatch();
    const pokeInfo = useSelector(state => state.pokemonDetailState.pokeInfo);
    const isAPILoading = useSelector(state => state.pokemonDetailState.isLoading);
    const isAPIFetched = useSelector(state => state.pokemonDetailState.isFetched);
    const isAPIError = useSelector(state => state.pokemonDetailState.isError);
    const isImageLoaded = useSelector(state => state.pokemonDetailState.isImageLoaded);
    const isOnCatching = useSelector(state => state.pokemonStorageState.catchOnProgress);
    const lstOfFetched = useSelector(state => state.pokemonStorageState.ownedPokemon);

    useEffect(() => {
        // dispatch(fetchAllPokemon());
        if (isAPILoading == false && isAPIFetched == false) {
            dispatch(fetchPokeDetailByFullUrl(url));
        }
    }, [isAPILoading, dispatch]);

    if (isAPILoading) {
        return (<View><Text>Loading....</Text></View>)
    } else if (isAPIError) {
        return (<View><Text>Failed!</Text></View>)
    } else if (isAPIFetched) {
        navigation.setOptions({
            title: pokeInfo.name ? pokeInfo.name.replace(/./, (e) => {return e.toUpperCase()}) : "Fetching..."
        })
        let numberOfOwned = lstOfFetched.filter((e) => e.id == pokeInfo.id).length;

        let pokeTypes = null;
        for (let types of pokeInfo.types) {
            if (!pokeTypes) {
                pokeTypes = types.type.name.replace(/./, (e) => {return e.toUpperCase()});
            } else {
                pokeTypes += ", " + types.type.name.replace(/./, (e) => {return e.toUpperCase()});
            }
        }
        return (
            <ScrollView>
                <View style={{...GlobalStyle.paddingMedium}}>
                    <Text
                        style= {{
                            ...GlobalStyle.centerText,
                            ...GlobalStyle.bigHeaderText
                        }}
                        
                    >{pokeInfo.name.replace(/./, (e) => {return e.toUpperCase()})}</Text>
                    <View style={{flexDirection: "column", width: "100%", alignContent:"center", justifyContent:"center", alignItems:"center"}}>
                        <Text style={{display: isImageLoaded ? "none" : "flex"}}>
                            Fetching images...
                        </Text>
                        <Image
                            style={{
                                ...GlobalStyle.detailsImage,
                                display: isImageLoaded ? "flex": "none"
                            }}
                            onLoadEnd={()=>{
                                dispatch(setImageLoaded(true));
                            }}
                            source={{uri: pokeInfo.image_url}}/>
                    </View>
                    <View style={{...GlobalStyle.paddingMedium}}>
                        <Text style= {{
                            ...GlobalStyle.centerText,
                            ...GlobalStyle.bigHeaderText
                        }}>Types</Text>
                        <Text style={{...GlobalStyle.centerText, ...localCss.statsHeader, ...GlobalStyle.blackColor}}>{pokeTypes}</Text>
                    </View>
                    <View style={{...GlobalStyle.paddingMedium}}>
                        <Text style= {{
                            ...GlobalStyle.centerText,
                            ...GlobalStyle.bigHeaderText
                        }}>Stats</Text>
                    </View>
                    <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                        <View style={localCss.statsSingle}><Text style={localCss.statsHeader}>{pokeInfo.stats[0].stat.name.toUpperCase()}</Text><Text style={GlobalStyle.blackColor}>{pokeInfo.stats[0].base_stat}</Text></View>
                        <View style={localCss.statsSingle}><Text style={localCss.statsHeader}>{pokeInfo.stats[1].stat.name.toUpperCase()}</Text><Text style={GlobalStyle.blackColor}>{pokeInfo.stats[1].base_stat}</Text></View>
                        <View style={localCss.statsSingle}><Text style={localCss.statsHeader}>{pokeInfo.stats[2].stat.name.toUpperCase()}</Text><Text style={GlobalStyle.blackColor}>{pokeInfo.stats[2].base_stat}</Text></View>
                        <View style={localCss.statsSingle}><Text style={localCss.statsHeader}>{pokeInfo.stats[3].stat.name.toUpperCase()}</Text><Text style={GlobalStyle.blackColor}>{pokeInfo.stats[3].base_stat}</Text></View>
                        <View style={localCss.statsSingle}><Text style={localCss.statsHeader}>{pokeInfo.stats[4].stat.name.toUpperCase()}</Text><Text style={GlobalStyle.blackColor}>{pokeInfo.stats[4].base_stat}</Text></View>
                        <View style={localCss.statsSingle}><Text style={localCss.statsHeader}>{pokeInfo.stats[5].stat.name.toUpperCase()}</Text><Text style={GlobalStyle.blackColor}>{pokeInfo.stats[5].base_stat}</Text></View>
                    </View>
                    <View
                        style={{
                            display: isOnCatching ? "flex" : "none" 
                        }}>
                        <Text style={{...GlobalStyle.centerText, ...GlobalStyle.paddingtblarge, ...GlobalStyle.blackColor}}>Catching is in progress</Text>
                    </View>
                    <TouchableNativeFeedback
                        onPress={(e) => {
                            if (!isOnCatching) 
                                dispatch(tryCatchingPokemons(pokeInfo));{
                            }
                        }}>
                            <View style={{
                                display: isOnCatching ? "none" : "flex",
                                ...GlobalStyle.mainColor}}>
                                <Text style={{...GlobalStyle.centerText, ...GlobalStyle.mainColorText, ...GlobalStyle.paddingtblarge}}>Catch Pokemon Use Pokeball</Text>
                            </View>
                    </TouchableNativeFeedback>
                    <View style={{...GlobalStyle.paddingMedium, paddingBottom: 30, display: numberOfOwned > 0 ? "flex" : "none"}}>
                        <Text style= {{
                            ...GlobalStyle.centerText,
                            ...GlobalStyle.bigHeaderText
                        }}>You have {numberOfOwned} of this pokemon</Text>
                        <Button
                            onPress={() => {
                                dispatch(removeOwnedPokemon(data.id))
                            }}
                            title="Release Pokemons"
                            color="#000"
                        />
                    </View>
                </View>
            </ScrollView>
            
        )
    }
}